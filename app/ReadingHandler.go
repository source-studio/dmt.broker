/*
 Broker application package. Handles the core application logic of the broker. The broker's role is to provide
 a comunnication bridge between the multiple satellites and terminal/monitors available. It will also serve as a central
 database to store all reading data.
*/
package app

import (
	"gxs3/demeter/common/app"
	dd "gxs3/demeter/common/data"
	"gxs3/demeter/common/model"
	gd "gxs3/godfather/data"
)

/*
 Handler implementation for reading requests. It basically adds readings to the data store as they are collected by
 different satellites
*/
type ReadingHandler struct {
	*app.Handler
	dao *dd.ReadingDao // Database access object used to make database transactions for readings
}

/*
 Creates a new reading handler. Requires the router handling communication, a sql template for database trnasactions
 and the query dictionary
*/
func NewReadingHandler(router *app.Router, template *gd.SqlTemplate, dict map[string]string) (*ReadingHandler, error) {
	var handler, err = app.NewHandler(router)

	if err != nil {
		return nil, err
	}

	dao, err := dd.NewReadingDao(template, dict)

	if err != nil {
		return nil, err
	}

	var rh = &ReadingHandler{handler, dao}
	return rh, nil
}

/*
 Initializes the reading handler. Registers it with the router in order to handle reading add requests
*/
func (rh *ReadingHandler) Init() {
	rh.Register(model.REQUEST_MT, app.READING_ADD, rh.add)
}

/*
 Reading add request handler. Sends a response to the requesting party, via the router to confirm the addition.
*/
func (rh *ReadingHandler) add(message []byte) error {
	var request, err = model.DecodeRequest(message, model.DecodeReading)

	if err != nil {
		var response = request.MakeResponse(app.BAD_REQUEST, nil)
		var respmesg, _ = response.Encode(nil)
		rh.Send(respmesg)
		return nil
	}

	var reading, ok = request.Body().(*model.Reading)

	if !ok {
		var response = request.MakeResponse(app.BAD_REQUEST, nil)
		var respmesg, _ = response.Encode(nil)
		rh.Send(respmesg)
		return nil
	}

	id, err := rh.dao.Add(reading)

	if err != nil {
		return err
	}

	var status uint = app.CREATED

	if id < 1 {
		status = app.NOT_FOUND
	}

	var response = request.MakeResponse(status, id)
	message, err = response.Encode(nil)

	if err != nil {
		return err
	}

	rh.Send(message)
	return nil
}
