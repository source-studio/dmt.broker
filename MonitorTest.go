package main

import (
	"fmt"
	"gxs3/demeter/common/net"
)

func main2() {
	worker := net.NewWorker("tcp://localhost:5555", "echo", false)
	defer worker.Close()
	reply := [][]byte{}
	var cont int64

	for {
		request := worker.Recv(reply)
		reply = request

		//fmt.Println(string(request[0]))
		if cont++; cont%100 == 0 {
			fmt.Println("Requests processed:", cont)
		}

		if len(request) == 0 {
			break
		}
	}
}
