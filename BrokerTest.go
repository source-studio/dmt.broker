package main

import (
	"fmt"
	"gxs3/demeter/common/net"
)

func main() {
	broker := net.NewBroker("tcp://*:5555", false)
	defer broker.Close()

	fmt.Println("Running broker...")
	broker.Run()
}
